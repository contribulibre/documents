Statuts type : http://www.cg47.fr/fileadmin/Documents/Culture%20et%20Patrimoine/Asso_47/Fiches_pratiques/Statuts.pdf

url gitlab : https://git.1twitif.fr/social-privacy/wikis/concept-asso et https://git.1twitif.fr/social-privacy/wikis/legal

Int�r�t g�n�ral : http://vosdroits.service-public.fr/particuliers/F426.xhtml#N10145
- �tre � but non lucratif,
- avoir un objet social et une gestion d�sint�ress�e,
- ne pas fonctionner au profit d'un cercle restreint de personnes.

#Statuts V1 :

##1. D�nomination
La d�nomination l�gale de l'association est ContribuLibre.

##2. Objet
Cr�ation, promotion et financement de projets � buts non lucratifs et libres de droits (ou compatible GPL).

##3. Si�ge social
B�gles pour avoir un soutien de la mairie.

##4. Membres
Peut �tre membre de l'association toute personne physique ou morale.

##x. Prises de d�cision  // vote --> 9. prise de d�cision
Chaque membre a le droit de vote aux exceptions suivantes :

- Pour assurer une certaine stabilit� dans les directions de l'association, si plus de 20 % des membres prenant part a une d�cision sont dans l'association depuis moins d'un an, leurs votes ne sont pas comptabilis�. Ils gardent cependant un avis consultatif.

En dessous de ce seuil, tout les votes sont pris en compte quel que soit l'anciennet� des votants.

- Pour assurer une certaine stabilit� dans la direction de l'assocaition, la mesure suivante est appliqu�e lors des prises de d�cisions :

si plus de 20% des membres participant au vote ont une anciennet� de moins d'un an, le vote des 20% n'est pas comptabilis�.

##5. Admission
Pour faire partie de l'association, il faut adh�rer aux  pr�sents statuts, au r�glement int�rieur

et remplir un bulletin  d'adh�sion en ligne. Aucune cotisation n'est n�cessaire.

Afin d'�viter les membres ficitfs, certains justificatifs peuvent �tre demand�s.





##6. Radiation
La qualit� de membre se perd par :
- La d�mission
- Le d�c�s.
- La radiation d�cid�e collectivement par vote, avec impossibilit� de se r�inscrire durant une periode d�cid�e lors du vote.
- Une inactivit� totale pendant une dur�e d�finie dans le r�glement int�rieur.
- Incapacit� � prouver son identit�.

##7. Ressources
Les ressources de l'association proviennent de:
- dons (financement participatif et autres)
- m�c�nats priv�s
- subventions publiques
- toute autre ressource autoris�e par la loi

Chaque ressource peut �tre attribu� :
- � l'association globalement
- � un projet port� par l'association
- � une sous partie d'un projet (y compris une tache individuelle).

L'association peut disposer de ressources non monn�taire. Les contribution en nature :
- travail r�alis�
- mise a disposition de materiel.

##8. Responsables legaux
Deux responsables l�gaux sont �lus au consensus ou � d�faut au bulletin secret.

En  cas de manque de candidat, les postes vacants seront tir�s au sort  parmi les membres, qui pourra refuser leur nomination entrainant un  nouveau tirage au sort parmi les membres qui n'ont pas refus�.  Si moins de deux (ben ca fait qu'un seul membre... xD) membres acceptent d'�tre responsables l�gaux, les  responsables sortants restent en poste avec tout pouvoir pour g�rer la  dissolution de l'association sous 3 mois.

Les  responsables l�gaux ont un droit de v�to sur toutes d�cisions, sauf sur  le changement de responsable l�gal. Tant qu'ils sont en poste, ils ont  acc�s au compte en banque et � l'administration du site.

Ils peuvent annoncer leur d�mission � tout moment. Leur d�mission sera effective apr�s �lection de leur rempla�ant(s).

Tout  membre peut demander anonymement et une fois par mois maximum, un  renouvellement des responsables l�gaux, ce qui entrainera une nouvelle  �lection de ces deux postes.





##9. Prise de d�cision
Toutes les d�cisions sont prises par consensus, si aucun  consensus n'en ressort apr�s une dur�e d�finie par le r�glement  int�rieur, on effectue un vote de valeur � bulletin secret. Dans le  cadre de la d�signation des responsables l�gaux se r�f�rer � l'article  responsables l�gaux.

Les prises de d�cision porteront sur :
- La modification des statuts
- L'�diction et la modification du r�glement int�rieur
- L'allocation des budgets de fonctionnement
- La radiation d'un membre
- L'attribution des r�les

La validation de la d�cision :

Chaque d�cision est soumise � quorum, diff�rent selon le type de d�cision et d�finie au sein du r�glement int�rieur.

Pour laisser le temps � chaque membre de s'exprimer, la prise de d�cision s'�tale sur au moins une semaine.

Chaque  participant laissera une trace �lectronique de sa participation � la  prise de d�cision sans qu'aucune indication de son vote ne soit  conserv�e.

##10. Transparence
Les d�cisions prises et en cours de votation sont publi�es et accessibles � tout moment, et ce, dans le respect du secret des votes.

La transparence de la comptabilit� se traduit par :

- l'accessibilit� d'une comptabilit� analytique en temps r�el (mise � jour automatiquement)

- la publication r�guli�re (d'une p�riode d�finie dans le r�glement int�rieur) de bilans comptables  et de la situation bancaire de l'association.





##X. En cas d'indisponnibilit� des outils techniques n�cessaires aux prises de d�cision en ligne
Tout membre peux proposer une liste de propositions � d�batre et trancher (ordre du jour) en r�union exeptionnelle (Assembl�e G�n�rale Exeptionnelle)

Une convocation doit e





#R�glement interieur V1 :
� concevoir et r�diger

##x. Admission







